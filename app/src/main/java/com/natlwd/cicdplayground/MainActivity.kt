package com.natlwd.cicdplayground

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.natlwd.cicdplayground.BuildConfig.ENV
import com.natlwd.cicdplayground.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.textView.text = "variant: ${ENV} \nbuild type: ${BuildConfig.BUILD_TYPE}\nversion: ${BuildConfig.VERSION_NAME}(${BuildConfig.VERSION_CODE})"
        //Testing
    }
}